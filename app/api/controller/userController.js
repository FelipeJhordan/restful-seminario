let fs = require("fs")

class UserController {
  getUsers(req, res) {
    fs.readFile("user.json", "utf8", function (err, data) {
      if (err) {
        var response = { status: "falha", resultado: err };
        res.json(response);
      } else {
        var obj = JSON.parse(data);
        var result = [];

        obj.usuarios.forEach(function (usuario) {
          if (usuario != null) {
            result.push(usuario);
          }
        });

        var response = { status: "sucesso", resultado: result };
        res.json(response);
      }
    });
  }

  getUser(req, res) {
    fs.readFile("user.json", "utf8", function (err, data) {
      if (err) {
        var response = { status: "falha", resultado: err };
        res.json(response);
      } else {
        var obj = JSON.parse(data);
        var result = "Usuário não encontrado";

        obj.usuarios.forEach(function (usuario) {
          if (usuario != null) {
            if (usuario.usuario_id == req.params.id) {
              result = usuario;
            }
          }
        });

        var response = { status: "sucesso", resultado: result };
        res.json(response);
      }
    });
  }

  saveUser(req, res) {
    fs.readFile("user.json", "utf8", function (err, data) {
      if (err) {
        var response = { status: "falha", resultado: err };
        res.json(response);
      } else {
        var obj = JSON.parse(data);
        req.body.usuario_id = obj.usuarios.length + 1;

        obj.usuarios.push(req.body);

        fs.writeFile("user.json", JSON.stringify(obj), function (err) {
          if (err) {
            var response = { status: "falha", resultado: err };
            res.json(response);
          } else {
            var response = {
              status: "sucesso",
              resultado: "Registro incluso com sucesso",
            };
            res.json(response);
          }
        });
      }
    });
  }

  editUserComplete(req, res) {
    try {
        fs.readFile("user.json", "utf8", function (err, data) {
      if (err) {
        var response = { status: "falha", resultado: err };
        res.json(response);
      } else {
        var obj = JSON.parse(data);

        obj.usuarios[req.body.usuario_id - 1].nome = req.body.nome;
        obj.usuarios[req.body.usuario_id - 1].site = req.body.site;

        fs.writeFile("user.json", JSON.stringify(obj), function (err) {
          if (err) {
            var response = { status: "falha", resultado: err };
            res.json(response);
          } else {
            var response = {
              status: "sucesso",
              resultado: "Registro editado com sucesso",
            };
            res.json(response);
          }
        });
      }
    });
    } catch (e) {
        res.json({error: "Erro"})
    }
  }

  deleteUser(req, res) {
    fs.readFile("user.json", "utf8", function (err, data) {
      if (err) {
        var response = { status: "falha", resultado: err };
        res.json(response);
      } else {
        var obj = JSON.parse(data);

        delete obj.usuarios[req.body.usuario_id - 1];

        fs.writeFile("user.json", JSON.stringify(obj), function (err) {
          if (err) {
            var response = { status: "falha", resultado: err };
            res.json(response);
          } else {
            var response = {
              status: "sucesso",
              resultado: "Registro excluído com sucesso",
            };
            res.json(response);
          }
        });
      }
    });
  }
}

module.exports = new UserController();

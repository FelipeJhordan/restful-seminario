var router = require("express").Router()
const userController = require("../controller/userController")

router.get('/api',userController.getUsers.bind(userController));

router.get('/api/:id', userController.getUser.bind(userController));

router.post('/api', userController.saveUser.bind(userController) );

router.put('/api', userController.editUserComplete.bind(userController));

router.delete('/api', userController.deleteUser.bind(userController));


module.exports = router

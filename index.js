const express = require('express')
const app = express()
const routes = require("./app/api/routes/routes")

app.use(express.json())
app.use(express.urlencoded( {extended: true}))

app.use(routes)

app.listen(3333, () => {
    console.log("Server ON")
})
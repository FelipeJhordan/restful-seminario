# Restful Seminario

**Descrição**: 
- Uma aplicação para demostrar como criar/utilizar um api restful.

**Tecnologias**:
- Node.js
- Express

**Endpoints**:
- get localhost:3333/api para mostrar todos usuários
- get localhost:3333/api/:id para mostrar um usuário específico
- post localhost:3333/api para criar um usuário novo
- put localhost:3333/api para alterar um usuário
- delete localhost:3333/api para deletar um usuário

**Modelo de usuário**:
```
{ 
  nome: "Fulano de tal",
  email: "fulano@uol.com.br",
  userid: 3
}
```


**Armazenamento**: 
- Json

**Ambiente**:
- Node: 12.18.3
- Windows 10/Debian
- NPM/Yarn


**Comandos**:
- npm run start 
- npm run dev ( requer nodemon na máquina)

